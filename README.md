# Jonathan's Features

> Added a basic admin panel, with basic authentication, a custom built table with search and sorting, and a custom built CSS library.

## Build Setup

``` bash
#add the following packages
npm install --save apollo-boost apollo-server-express apollo-link-context vue-apollo graphql graphql-tools cors subscriptions-transport-ws apollo-link-ws lodash vue-router moment

# install dependencies
npm install

# Connect up to the backend
npm install -g graphcool
graphcool pull

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
