import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '../components/HelloWorld'
import AppLogin from '../components/AppLogin'
import Admin from '../components/Admin'
import UpdateUser from '../components/UpdateUser'
import ReadUser from '../components/ReadUser'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: HelloWorld
    },
    {
      path: '/login',
      component: AppLogin
    },
    // ------------------------------------------ Admin ----------------------------------
    {
      path: '/admin',
      component: Admin
    },
    {
      path: '/user/:id',
      component: ReadUser
    },
    {
      path: '/user/update/:id',
      component: UpdateUser,
      name: 'updateUser'
    }
  ],
  // set mode to ‘history’ to remove the hash from the URLs
  mode: 'history'
})